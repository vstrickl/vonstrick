import React from "react"
import { graphql } from "gatsby"

import "../styles/styles.scss"

const md = (data) => {
  const { frontmatter, html } = data.data.allMarkdownRemark.edges[0].node
  console.log(frontmatter.title)
  return (<div>
    <div class="header">
      <div class="full-name">
        <span class="first-name">{frontmatter.first}</span>
        <span class="last-name">{frontmatter.last}</span>
      </div>
      <h1>{frontmatter.job}</h1>
      <div class="contact-info">
        <span class="email-val"><a href={"mailto:" + frontmatter.email}>{frontmatter.email}</a></span>
        <p>{frontmatter.location}</p>
      </div>
    </div>
    <div
      dangerouslySetInnerHTML={{ __html: html }}
    />
  </div>)
}

const IndexPage = ({ data }) => (
  <div class="container">
    {md({ data })}
  </div>
)

export const query = graphql`
query {
  allMarkdownRemark(
    filter: {fileAbsolutePath: {regex: "/resume/"}}
  ) {
      edges {
        node {
          frontmatter {
            first
            last
            location
            email
            job
          }
          html
        }
      }
    }
}
`

export default IndexPage