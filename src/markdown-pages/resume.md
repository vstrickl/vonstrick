---
slug: "/resume"
first: "Vonique"
last: "Stricklen"
location: "Northern California (Silicon Valley)"
email: "vstrickl.git@gmail.com"
job: Web Developer
---

## SUMMARY
Front-End web developer adept in building consumer facing static (Express) and dynamic (WordPress) websites. Skilled JavaScript and Node.js programmer with a fine eye for aesthetics. Charismatic troubleshooter that offers clear and simple solutions for complex problems.  


## EXPERIENCE  

### Self-Employed - Web Developer
Sep 2018 - Present  
[vonfitbjj.com](http://vonfitbjj.com/) | [blog](http://blog.vonfitbjj.com/) | [mobile](http://m.vonfitbjj.com/)   

Hard coded customer facing website with Express Node.js. Utilized Social Media marketing skills to drive consumer interest. Updated Blog with WordPress Content Management System. Introduced Divi to the company to improve the look and feel of content. Added Shopify to create an online eCommerce store for customers to purchase personal training and private Brazilian JiuJisu sessions. Maintained changes and streamed lined workflow using the Git version control system.  
  
  
### TIBCO - Web Master (Docsite)
Aug 2013 - Dec 2013  

Revamped [TIBCO's Cloud docs site](https://integration.cloud.tibco.com/docs/index.html) utilizing JavaScript, jQuery, HTML, CSS and Liquid syntax knowledge. Implemented and enhanced the automated release process and content management system for publishing internal software and technical documentation to TIBCO's docsite. Established the automated editing review process for technical documents based on IBM style. Increased clarity, response time and efficiency during publication time for websites, web producers, product managers, fulfillment and release teams.  
  
  
### Coaches Aid - Marketing Specialist (Freelance)
Aug 2013 - Dec 2013  

Increased online traffic to Coaches Aid live broadcast by engaging with spectators using sales techniques. Boosted sponsor confidence with Coaches Aid product by connecting and interacting with sponsors and the viewers through use of social media, photography and general feedback. Increased sales by utilizing the live broadcast and still slides to attract customers to Coaches Aid table.  
  
  
### Project Censored - Web Developer/Research Intern (SF State Affiliate)
Sep 2012 - Dec 2013  

Updated styles and components for Projects Censored's CMS. Hard coded functions and features using FTP, HTML, CSS and PHP. Produced, managed, organized and marketed content as a web producer for affiliate websites. Researched, edited, fact checked and analyzed stories to produce newsworthy articles for the book "Censored 2014". Guided intern research and development for story ideas. Organized and managed daily meetings, and assisted in the creation of a movie sneak peak for Project Censored's documentary as president of Project Censored - SF State Affiliate since February 2013.  

## EDUCATION
Aug 2011 - Dec 2013  
San Francisco State University  
San Francisco, CA  
B.A Print/Online Journalism  

## SKILLS
Web/Website Development,
JavaScript,
jQuery,
Node.js,
HTML,
CSS,
Sass,
SQL,
NoSQL,
IIS,
PHP,
WordPress,
Divi,
Jekyll,
Git,
Shopify,
.svg,
Web Content Management,
WCM, CMS, WCMS,
CMD,
Powershell,
Computer Networking,
Visual Studio,
Adobe Illustrator,
Adobe Photoshop,
Project Management,

## PROJECTS 
#### Rolling for the People - Co-Creator/Contributor   
**Nov 2016 - Jan 2017**  
Raise $2k for the ACLU and Human Rights.  
[rollingforthepeople.com](http://www.rollingforthepeople.com/)   

#### Valor Online News - Videographer
**Dec 2012**  
Created mini-documentary which was featured in [Graciemag.com](http://bit.ly/2Cm2wtU).    
[VIDEO:The Power of JiuJitsu](https://youtu.be/HqnQT1cNmfo)
